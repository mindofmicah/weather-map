@extends('_layouts.master')

<?php
$width = 7;
$fontsize = floor(50 / $width);


function gradientFromMinMax($min, $max)
{
    return "linear-gradient(135deg, {$max} 50%, {$min} 50%)";
}

?>
@section('body')
    <h1 style="background: antiquewhite; text-align: center; padding: 5px 10px; border-bottom:6px ridge rebeccapurple">
        High temperatures of STL in 2020</h1>
    <div style="display: grid; grid-template-columns: repeat({{$width}}, 1fr); grid-template-rows:repeat({{$width}},1fr); border-collapse: collapse; border:3px ridge rebeccapurple">
        @foreach($page->cells as $cell)
            <div style="border:1px solid #333; text-align: center;padding-top:100%;position:relative;">
                <div style="position:absolute;top: 0; font-size:{{$fontsize/3}}vw;left: 0;width: 100%;height: 100%; display:flex; justify-content: space-around; flex-direction: column; background:{{gradientFromMinMax($cell['min']['hex'], $cell['max']['hex'])}}">
                    <div style="position: absolute;top:0;right:0; font-size:.5em; background-color:antiquewhite; padding:.1em;padding-right:.2em">{{$cell['date']}}</div>
                    <div style="text-align: left;padding-left:.1em;">
                        <div style="font-size:.7em;">{{$cell['max']['color']}}</div>
                        <div>{{$cell['max']['value']}}&deg;</div>
                    </div>
                    <div style="text-align: right; padding-right:.2em;">
                        <div>{{$cell['min']['value']}}&deg;</div>
                        <div style="font-size:.7em;">{{$cell['min']['color']}}</div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection