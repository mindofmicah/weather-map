@extends('_layouts.master')

<?php
use WeatherMap\SpiralValues;
$cells = (new SpiralValues)->spiralOut($page->cells, ['date' => '', 'value' => '?']);


$width = sqrt(count($cells));
$fontsize = floor(50 / $width);
?>
@section('body')
    <h1 style="background: antiquewhite; text-align: center; padding: 5px 10px; border-bottom:6px ridge rebeccapurple">
        High temperatures of STL in 2020</h1>
    <div style="display: grid; grid-template-columns: repeat({{$width}}, 1fr); grid-template-rows:repeat({{$width}},1fr); border-collapse: collapse; border:3px ridge rebeccapurple">
        @foreach($cells as $cell)
            <div style="border:1px solid #333; text-align: center;padding-top:100%;position:relative;">
                <div style="position:absolute;top: 0; font-size:{{$fontsize/3}}vw;left: 0;width: 100%;height: 100%; background: {{(!empty($cell['max'])?$cell['max']['hex']:'antiquewhite')}}; display:flex; justify-content: space-around; flex-direction: column">
                    <div>{{$cell['date']}}</div>
                    <div style="font-size:{{$fontsize}}vw;">{{($cell['max']??$cell)['value']}}</div>
                    <div>{{($cell['max']??$cell)['color']??''}}</div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
