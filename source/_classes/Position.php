<?php
namespace WeatherMap;

class Position {
    public $x, $y;

    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function down()
    {
        $this->x++;
    }

    public function up()
    {
        $this->x--;
    }

    public function left()
    {
        $this->y--;
    }

    public function right()
    {
        $this->y++;
    }
};


