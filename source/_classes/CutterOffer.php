<?php
namespace WeatherMap;

use WeatherMap\Position;

class CutterOffer
{
    private $thresholds = [];
    private $values = [];

    public function __construct($min, $max, $step)
    {
        $this->thresholds = range($min, $max, $step);
        array_unshift($this->thresholds, -INF);
    }
    public function addValuesForThresholds(array $values)
    {
        if (count($values) !== count($this->thresholds)) {
            throw new \Exception('cannot add fewer/greater amount of values');
        }
        $this->values = $values;
    }

    public function getValueForThreshold($value)
    {
        foreach ($this->thresholds as $index => $cutoff) {
            if ($cutoff <= $value) {
                $ii = $index;
            }
        }
        return $this->values[$ii];
    }
}
