<?php
namespace WeatherMap;

use WeatherMap\Position;

class SpiralValues
{
    public function spiralOut($values, $default_value = null)
    {
        $box_size = ceil(sqrt(count($values)));
        if ($box_size % 2 === 0) {
            $box_size++;
        }
        $ret = array_fill(0, $box_size**2, $default_value);

        $center = floor($box_size / 2);
        $position = new Position($center, $center);

        $index = intval($position->y + ($position->x * $box_size));
        $counter =0;
        $ret[$index] = $values[$counter++];

        $MAX_RUNS = 100;
        $how_many = 1;
        $direction_count = 0;
        $directions = ['up', 'right', 'down', 'left'];
        
        while (true && $counter< count($values)) {
            $direction = $directions[$direction_count % 4];
            for ($j = 0; $j < $how_many; $j++) {
                $position->$direction();

                    $index = intval($position->y + ($position->x * $box_size));
                    if (!$values->has($counter)) {
//                        dd($counter, array_keys($values));
                        break 2;

                    }
                    $ret[$index] = $values[$counter++] ?? '00/00';
//                    dump($index);//dd($position);
//                    placeValue($cells, $position, \Carbon\Carbon::createFromFormat('z', $counter)->format('m/d'), $observations[$counter++] ?? '?', $cutoffs);
            }

            if ($direction_count % 2) {
                $how_many++;
            }

            $direction_count++;
        }

        return $ret;

        dd(get_defined_vars());
    }
}
