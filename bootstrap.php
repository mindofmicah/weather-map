<?php
use WeatherMap\SpiralValues;
use WeatherMap\CutterOffer;
use Carbon\Carbon;
use Illuminate\Support\Collection;

use TightenCo\Jigsaw\Jigsaw;

/** @var $container \Illuminate\Container\Container */
/** @var $events \TightenCo\Jigsaw\Events\EventBus */

$events->beforeBuild(function (Jigsaw $jigsaw) {
    $colors = $jigsaw->getConfig('colors')->flatten()->all();

    $dmc_colors = fetchColorCodes($colors)->pluck('color_hexcode', 'code');
    $colors = array_map(function ($color) use ($dmc_colors) {
        return ['color' => $color, 'hex' => '#' . $dmc_colors[$color]];
    }, $colors);

    $CURRENT_YEAR = date('Y');
    $observations = collect();
    foreach (array_chunk(range(0, date('z')), 30) as $i => $chunk) {
        $start_date = Carbon::createFromFormat('Y z', $CURRENT_YEAR . ' ' . current($chunk))->format('Ymd');
        $end_date = Carbon::createFromFormat('Y z', $CURRENT_YEAR . ' ' . end($chunk))->format('Ymd');

        $api = 'https://api.weather.com/v1/location/KSTL:9:US/observations/historical.json?apiKey=' . env('WEATHER_API_TOKEN') . '&units=e&startDate=' . $start_date . '&endDate=' . $end_date . '';
        $api_contents = json_decode(file_get_contents($api));
        $observations = $observations->merge(collect($api_contents->observations)->groupBy(function ($observation) {
            return Carbon::createFromTimestamp($observation->valid_time_gmt, 0)
                ->setTimezone('America/Chicago')
                ->format('m-d');
        })->map(function (Collection $observations) {
            return [
                'min' => $observations->min('temp'),
                'max' => $observations->max('temp'),
            ];
        })->values());
    }

    $cutter = new CutterOffer(30, 100, 5);
    $cutter->addValuesForThresholds($colors);

    $values = $observations->map(function ($temp, $index) use ($CURRENT_YEAR, $cutter) {
        $temp['date'] = Carbon::createFromFormat('Y z', $CURRENT_YEAR . ' ' . $index)->format('m/d');
        $temp['min'] = $cutter->getValueForThreshold($temp['min']) + ['value' => $temp['min']];
        $temp['max'] = $cutter->getValueForThreshold($temp['max']) + ['value' => $temp['max']];
        return $temp;
    });

    $jigsaw->setConfig('cells', $values);
});

function fetchColorCodes(array $colors)
{
    return collect(json_decode(file_get_contents('https://stitching.whamdonk.com/api/floss-colors/' . implode(',', $colors))));
}

